## How to run?
    1. npm i
    2. check .env file maybe need something to change

## Routes
    Auth endpoints
    
    1. Signin   [POST]  /auth/signin
    2. Signup   [POST]  /auth/signup

    Note endpoints
    
    1. Create   [POST]  /note/
    2. List     [GET]   /note/
    3. Update   [PUT]   /note/:id
    4. Delete   [DEL]   /note/:id
    5. Share    [PUT]   /note/:id/_share
    5. Get note [GET]   /note/_shared/:uuid | you will get "uuid" from Share endpoint.
    
    
