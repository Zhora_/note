const Router = require('express').Router;

module.exports = ({ app, authService }) => {
  const router = new Router();

  router.use(require('./signin')({ app, authService, Router }));

  router.use(require('./signup')({ app, authService, Router }));

  return router;
};
