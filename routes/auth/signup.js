const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = ({ app, authService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { expect, config, error } = app;

  router.post('/signup', async (req, res, next) => {
    try {
      let { username, password } = transform({
        data: req.body,
        expect,
        config,
        error
      });

      const checkUsername = await authService.findByUsername({ username });

      if (checkUsername) {
        throw new error(409, 'USERNAME_ALREADY_EXISTS');
      }

      password = await bcrypt.hash(password, 10);

      const { insertedId } = await authService.create({
        username,
        password,
        date_created: new Date()
      });

      const token = jwt.sign(
        { id: insertedId, username },
        config.jwt.secretKey,
        config.jwt.options
      );

      return res.json({ token });
    } catch (error) {
      next(error);
    }
  });

  return router;
};

function transform({ data: { username, password }, expect, config, error }) {
  expect(username)
    .param('username')
    .to.be.a('string')
    .and.to.match(new RegExp(config.username.valid_pattern, 'i'));

  username = username.toLowerCase();

  expect(password)
    .param('password')
    .to.be.a('string')
    .and.to.match(new RegExp(config.password.valid_pattern));

  return {
    username,
    password
  };
}
