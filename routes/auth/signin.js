const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = ({ app, authService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { expect, error, config } = app;

  router.get('/signin', async (req, res, next) => {
    try {
      let { username, password } = transform({
        data: req.body,
        expect,
        config
      });

      const user = await authService.findByUsername({
        username
      });

      if (!user) {
        throw new error('ERR_USER_NOT_FOUND');
      }

      if (!bcrypt.compare(password, user.password)) {
        throw new error('PASSWORD_OR_USERNAME_WRONG');
      }

      const token = jwt.sign(
        { id: user._id, username: user.username },
        config.jwt.secretKey,
        config.jwt.options
      );

      return res.json({
        token
      });
    } catch (error) {
      next(error);
    }
  });

  return router;
};

function transform({ data: { username, password }, expect, config }) {
  expect(username)
    .param('username')
    .to.be.a('string')
    .and.to.match(new RegExp(config.username.valid_pattern, 'i'));

  username = username.toLowerCase();

  expect(password)
    .param('password')
    .to.be.a('string')
    .and.to.match(new RegExp(config.password.valid_pattern));

  return {
    username,
    password
  };
}
