module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { error } = app;

  router.get('/', async (req, res, next) => {
    try {
      const { id: uuid } = req.params;

      const note = await noteService.findByHash(uuid);

      if (!note) {
        throw new error('Note not found');
      }

      return res.json({ note: note.note });
    } catch (error) {
      next(error);
    }
  });

  return router;
};
