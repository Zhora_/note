module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { expect, error, acl } = app;

  router.post('/', async (req, res, next) => {
    try {
      const { account, body } = req;

      //checking is logged or not
      acl.checkAuth(account, error);

      //getting clean data
      const data = transform({ expect, data: body, account });

      const { insertedId } = await noteService.create(data);

      return res.json({ id: insertedId });
    } catch (error) {
      next(error);
    }
  });

  return router;
};

function transform({ expect, data, account }) {
  const { note } = data;
  expect(note)
    .param('note')
    .to.be.a('string')
    .and.have.length.within(1, 1000);

  return {
    account_id: account.id,
    note,
    hash: [],
    date_created: new Date()
  };
}
