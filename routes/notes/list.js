module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { error, acl } = app;

  router.get('/', async (req, res, next) => {
    try {
      const { account } = req;

      //checking is logged or not
      acl.checkAuth(account, error);

      const notes = await noteService.findByAccountId(account.id);

      return res.json(notes);
    } catch (error) {
      next(error);
    }
  });

  return router;
};
