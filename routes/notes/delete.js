module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { error, acl } = app;

  router.delete('/', async (req, res, next) => {
    try {
      const { account } = req;
      const { id } = req.params;

      const note = await noteService.findById(id);

      if (!note) {
        throw new error('Note not found');
      }

      acl.checkOwnership({ targetAccountId: note.account_id, account, error });

      const { deletedCount } = await noteService.removeById(id);

      return res.json({ result: !!deletedCount });
    } catch (error) {
      next(error);
    }
  });

  return router;
};
