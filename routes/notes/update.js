module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { expect, error, acl } = app;

  router.put('/', async (req, res, next) => {
    try {
      const { account, body } = req;
      const { id } = req.params;

      const note = await noteService.findById(id);

      if (!note) {
        throw new error('Note not found');
      }

      acl.checkOwnership({ targetAccountId: note.account_id, account, error });

      const data = transform({ expect, data: body });

      const { modified } = await noteService.modifyById({ id, data });

      return res.json({ result: !!modified });
    } catch (error) {
      next(error);
    }
  });

  return router;
};

function transform({ expect, data }) {
  const { note } = data;

  expect(note)
    .param('note')
    .to.be.a('string')
    .and.have.length.within(1, 1000);

  return {
    note,
    date_modified: new Date()
  };
}
