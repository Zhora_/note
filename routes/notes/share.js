const { v4: uuidv4 } = require('uuid');

module.exports = ({ app, noteService, Router }) => {
  const router = new Router({ mergeParams: true });
  const { error, acl } = app;

  router.put('/_share', async (req, res, next) => {
    try {
      const { account } = req;
      const { id } = req.params;

      const note = await noteService.findById(id);

      if (!note) {
        throw new error('Note not found');
      }

      acl.checkOwnership({ targetAccountId: note.account_id, account, error });

      const hash = uuidv4();

      await noteService.addHashById({ id, hash });

      return res.json({ hash });
    } catch (error) {
      next(error);
    }
  });

  return router;
};
