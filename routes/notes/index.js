const Router = require('express').Router;

module.exports = ({ app, noteService }) => {
  const router = new Router();

  router.use(
    '/',
    require('./list')({ app, noteService, Router }),
    require('./create')({ app, noteService, Router })
  );

  router.use('/_shared/:id', require('./shared')({ app, noteService, Router }));

  router.use(
    '/:id',
    require('./update')({ app, noteService, Router }),
    require('./delete')({ app, noteService, Router }),
    require('./share')({ app, noteService, Router })
  );

  return router;
};
