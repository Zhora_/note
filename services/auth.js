class Auth {
  constructor({ mongodb }) {
    this._col = mongodb.collection('account');
  }

  async create(data) {
    return this._col.insertOne(data);
  }

  async findByUsername({ username }) {
    return this._col.findOne({
      username
    });
  }
}

module.exports = Auth;
