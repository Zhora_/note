const ObjectId = require('mongodb').ObjectID;

class Note {
  constructor({ mongodb }) {
    this._col = mongodb.collection('note');
  }

  async create(data) {
    return this._col.insertOne(data);
  }

  async findByAccountId(accountId) {
    return this._col.find({ account_id: accountId }).toArray();
  }

  async findById(id) {
    return this._col.findOne({ _id: ObjectId(id) });
  }

  async findByHash(hash) {
    return this._col.findOne({ hash: { $in: [hash] } });
  }

  async removeById(id) {
    return this._col.deleteOne({ _id: ObjectId(id) });
  }

  async modifyById({ id, data }) {
    let { result } = await this._col.updateOne(
      { _id: ObjectId(id) },
      { $set: data }
    );

    return {
      found: result.n !== 0,
      modified: result.nModified !== 0
    };
  }

  async addHashById({ id, hash }) {
    let { result } = await this._col.updateOne(
      { _id: ObjectId(id) },
      {
        $push: {
          hash
        }
      }
    );

    return {
      found: result.n !== 0,
      modified: result.nModified !== 0
    };
  }
}

module.exports = Note;
