module.exports = {
  password: {
    valid_pattern:
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*\\-\\_\\?])(?=.{8,20})',
    min: 8,
    max: 20
  },
  username: {
    valid_pattern: '^[-_a-z0-9]{6,30}$',
    min: 6,
    max: 30
  },
  jwt: {
    secretKey: 'somesecretkey',
    options: { expiresIn: '20m' }
  }
};
