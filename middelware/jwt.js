const jwt = require('jsonwebtoken');

module.exports = app => {
  return async (req, res, next) => {
    try {
      const authHeader = req.headers.authorization;

      if (authHeader) {
        const [, token] = authHeader.split(' ');

        const account = jwt.verify(token, app.config.jwt.secretKey);

        req.account = account;
      }

      next();
    } catch (e) {
      next(e);
    }
  };
};
