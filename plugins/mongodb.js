const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;

class MongoDb {
  constructor(props) {
    this._url = props.url;
    this._db = props.database;
    this._client = new MongoClient(this._url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  }

  async init() {
    if (this._mongo) {
      return this._mongo;
    }

    await this._client.connect();

    this._client.ObjectId = ObjectId;

    this._mongo = this._client.db(this._db);

    console.log('connected to mongodb');

    return this._mongo;
  }
}

module.exports = MongoDb;
