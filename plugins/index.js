const adapterFactory = require("./adapter/AdapterFactory");

class Plugins {
  init(app, plugins) {
    return new Promise((resolve, reject) => {
      for (let plugin in plugins) {
        app[plugin] = adapterFactory.builder(plugin, plugins[plugin]);
      }

      resolve(app);
    }).catch(err => {
      console.log(err);
    });
  }
}

module.exports = new Plugins();
