const fs = require("fs");
const path = require("path");
const HttpError = require("http-errors");

class AdapterFactory {
  builder(plugin, config) {
    let plugin_path = path.resolve(__dirname, `../${plugin}.js`);

    if (!fs.existsSync(plugin_path)) {
      throw new HttpError(404, "Plugin Not Found !");
    } else {
      const Plugin = require(`../${plugin}.js`);

      return new Plugin(config).init();
    }
  }
}

module.exports = new AdapterFactory();
