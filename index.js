require('dotenv').config();

const _app = require('./lib/App');
const http = require('http');
const express = require('express');
const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

//ROUTERS
const authRouter = require('./routes/auth/index');
const noteRouter = require('./routes/notes/index');

//MIDDELWARES
const jwtMiddleware = require('./middelware/jwt');

//SERVICES
const AuthService = require('./services/auth');
const NoteService = require('./services/note');

_app({
  app: express()
})
  .then(async app => {
    const authService = new AuthService({
      mongodb: app.mongodb
    });

    const noteService = new NoteService({
      mongodb: app.mongodb
    });

    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());

    app.use(jwtMiddleware(app));

    app.use('/auth', authRouter({ app, authService }));
    app.use('/note', noteRouter({ app, noteService }));

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      next(createError(404));
    });

    // error handler
    app.use(function(error, req, res, next) {
      console.log(error);
      return res.status(error.status || 500).json({ error });
    });

    const server = http.createServer(app);

    server.listen(process.env.PORT || '1021');
  })
  .catch(err => {
    console.log(err);
    app.log.fatal(err);
    process.exit(1);
  });

module.exports = _app;
