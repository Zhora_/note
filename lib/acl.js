function checkOwnership({ targetAccountId, account, error }) {
  //check logged or not
  checkAuth(account, error);

  if (targetAccountId !== account.id) {
    throw new error('Access denied!');
  }

  return true;
}

function checkAuth(account, error) {
  if (!account) {
    throw new error('Access denied!');
  }
}

module.exports = {
  checkOwnership,
  checkAuth
};
