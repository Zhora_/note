const Plugins = require('../plugins/index');
const chai = require('chai');
const chaiParam = require('chai-param');
const config = require('../config');
const httpError = require('http-errors');
const acl = require('./acl');

async function init({ app }) {
  const plugins = await Plugins.init(app, {
    mongodb: {
      url: process.env.MONGODB_URL,
      database: process.env.MONGODB_DB
    }
  });

  chai.use(chaiParam);

  Object.assign(app, {
    mongodb: await plugins.mongodb,
    expect: chai.expect,
    config,
    error: httpError,
    acl
  });

  return app;
}

module.exports = init;
